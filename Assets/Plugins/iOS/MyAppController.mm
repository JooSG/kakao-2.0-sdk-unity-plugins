//
//  GMO.m
//  Unity-iPhone
//
//  Created by Joo on 2015. 6. 22..
//
//

#import "UnityAppController.h"
#import <KakaoOpenSDK/KakaoOpenSDK.h>

@interface MyAppController : UnityAppController
@end

@implementation MyAppController
- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    [super application:application didFinishLaunchingWithOptions:launchOptions];
    KOSession *session = [KOSession sharedSession];
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([KOSession isKakaoAccountLoginCallback:url]) {
        return [KOSession handleOpenURL:url];
    }
    return [super application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [super applicationWillEnterForeground:application];
}

- (void)applicationDidBecomeActive:(UIApplication*)application
{
    [super applicationDidBecomeActive:application];
    [KOSession handleDidBecomeActive];
}
@end

IMPL_APP_CONTROLLER_SUBCLASS(MyAppController)