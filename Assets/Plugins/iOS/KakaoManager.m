//
//  GetBundleIdentifier.mm
//  Unity-iPhone
//
//  Created by Joo on 2015. 6. 22..
//
//

#import <Foundation/Foundation.h>
#import <KakaoOpenSDK/KakaoOpenSDK.h>
#import <KakaoGameSDK/KakaoGameSDK.h>
#import "KakaoManager.h"

extern UIViewController *UnityGetGLViewController(); // Root view controller of Unity screen.

@implementation KakaoManager

NSString *m_KaHeader;
NSString *m_AccessToken;

NSMutableDictionary *dict;
RegisteredFriendContext *_registeredFriendContext;
InvitableFriendContext *_invitableFriendContext;
NSMutableArray *m_InstalledfriendsInfo;
NSMutableArray *m_InvitablefriendsInfo;
NSInteger _limitCount;

char* UnityGameObjectName = "KakaoManager";
KOUser *_user;

char* _GetCFBundleVersion()
{
    NSString *version = [[NSBundle mainBundle] bundleIdentifier];
    return strdup([version UTF8String]);
}

void _KakaoInit()
{
    NSLog(@"_KakaoInit");
    dict =     [[NSMutableDictionary alloc] init];
    [dict removeAllObjects];
    
    KOSession *session = [KOSession sharedSession];
    _limitCount = 1000;
    _registeredFriendContext = [RegisteredFriendContext contextWithLimit:_limitCount];
    _invitableFriendContext = [InvitableFriendContext contextWithLimit:_limitCount];
    m_InstalledfriendsInfo = [[NSMutableArray alloc] init];
    m_InvitablefriendsInfo = [[NSMutableArray alloc] init];
    
    if(session.isOpen == true)
    {
        dict[@"success"] = @"true";
        m_KaHeader = [KOUtils kaHeader];
        m_AccessToken =  session.accessToken;
    }else
        dict[@"success"] = @"false";
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    UnitySendMessage(UnityGameObjectName, "KakaoInitResponse", MakeStringCopy([myString UTF8String]));
}

void _KakaoLogin()
{
    NSLog(@"KakaoLogin");
    
    [dict removeAllObjects];
    dict =     [[NSMutableDictionary alloc] init];
    KOSession *session = [KOSession sharedSession];
    
    //    session.presentingViewController = self.navigationController;
    session.presentingViewController =   UnityGetGLViewController();
    
    [session openWithCompletionHandler:^(NSError *error) {
        NSLog(@"%@",error.description );
        if(session.isOpen == true){
            dict[@"success"] = @"true";
            _limitCount = 1000;
            _registeredFriendContext = [RegisteredFriendContext contextWithLimit:_limitCount];
            _invitableFriendContext = [InvitableFriendContext contextWithLimit:_limitCount];
            m_InstalledfriendsInfo = [[NSMutableArray alloc] init];
            m_InvitablefriendsInfo = [[NSMutableArray alloc] init];
            m_KaHeader = [KOUtils kaHeader];
            m_AccessToken =  session.accessToken;
        }else{
            dict[@"success"] = @"false";
            dict[@"error"] = [NSString stringWithFormat:@"%ld", (long)error.code];
        }
        
        NSError * err;
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&err];
        NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        UnitySendMessage(UnityGameObjectName, "KakaoInitResponse", MakeStringCopy([myString UTF8String]));
    } authParams:nil authType:KOAuthTypeTalk];
    
}

void _KakaoLogOut()
{
    [dict removeAllObjects];
    dict =     [[NSMutableDictionary alloc] init];
    [KakaoGameAPI logoutAndCloseWithCompletionHandler:^(BOOL success, NSError *error) {
        NSLog(@"success : %d",success);
        NSLog(@"error : %@",error.description);
        NSLog(@"errorCode : %ld",(long)error.code);
        if(success == true)
            dict[@"success"] = @"true";
        else
        {
            dict[@"success"] = @"false";
            dict[@"error"] = [NSString stringWithFormat:@"%ld", (long)error.code];
        }
        
        NSError * err = NULL;
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&err];
        NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        UnitySendMessage(UnityGameObjectName, "KakaoLogOutResponse", MakeStringCopy([myString UTF8String]));
    }];
}

void _KakaoSignOut()
{
    [dict removeAllObjects];
    dict =     [[NSMutableDictionary alloc] init];
    [KakaoGameAPI unlinkTaskWithCompletionHandler:^(BOOL success, NSError *error) {
        if(success == true)
            dict[@"success"] = @"true";
        else
        {
            dict[@"success"] = @"false";
            dict[@"error"] = error.code;
        }
        NSError * err;
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&err];
        NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        UnitySendMessage(UnityGameObjectName, "KakaoSignOutResponse", MakeStringCopy([myString UTF8String]));
    }];
    
}

void _KakaoRequestMe()
{
    [dict removeAllObjects];
    dict =     [[NSMutableDictionary alloc] init];
    [KakaoGameAPI meTaskWithCompletionHandler:^(id result, NSError *error) {
        if (error) {
            NSLog(@"Error Description : %@",error.description);
            dict[@"success"] = @"false";
            dict[@"error"] = [NSString stringWithFormat:@"%ld", (long)error.code];
        } else {
            _user = result;
            NSMutableDictionary *kakaoUserProfile = [[NSMutableDictionary alloc] init];
            NSLog(@"keys : %@",[_user.properties allKeys]);
            NSLog(@"extra keys : %@",[_user.extras allKeys]);
            dict[@"success"] = @"true";
            kakaoUserProfile[@"id"] = _user.ID;
            kakaoUserProfile[@"nickname"] = RemoveEmoji([_user.properties valueForKey:@"nickname"]);
            kakaoUserProfile[@"uuid"] = _user.uuid;
            kakaoUserProfile[@"thumbnailImagePath"] = [_user.properties valueForKey:@"thumbnail_image"];
            kakaoUserProfile[@"profileImagePath"] = [_user.properties valueForKey:@"profile_image"];
            kakaoUserProfile[@"serviceUserId"] = [_user.extras valueForKey:@"EXTRA_KEY_SERVICE_USER_ID"];
            kakaoUserProfile[@"remainingInviteCount"] = [_user.extras valueForKey:@"EXTRA_KEY_INVITE_MESSAGE_REMAINING_COUNT"];
            kakaoUserProfile[@"remainingGroupMsgCount"] = [_user.extras valueForKey:@"EXTRA_KEY_GROUP_CHAT_MESSAGE_REMAINING_COUNT"];
            NSMutableDictionary *msg_blocked =[[NSMutableDictionary alloc] init];
            [msg_blocked setValue:[[NSString stringWithFormat:@"%@", [_user.properties valueForKey:@"msg_blocked"]]    isEqual: @"false"] ? @"false":@"true"forKey:@"msg_blocked"];
            
            kakaoUserProfile[@"properties"] = msg_blocked;
            dict[@"userprofile"] = kakaoUserProfile;
            NSError * err;
            NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&err];
            NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            myString = [myString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
            
            UnitySendMessage(UnityGameObjectName, "GetKakaoKAHeader", MakeStringCopy([m_KaHeader UTF8String]));
            UnitySendMessage(UnityGameObjectName, "GetKakaoAccessToken", MakeStringCopy([m_AccessToken UTF8String]));
            UnitySendMessage(UnityGameObjectName, "GetValidCheckAccessTokeURL", "https://game-kapi.kakao.com/v1/user/access_token_info");
            UnitySendMessage(UnityGameObjectName, "KakaoRequestMeResponse", MakeStringCopy([myString UTF8String]));
        }
    }];
}

void _KakaoRequestInstalledFriendInfo()
{
    [dict removeAllObjects];
    dict = [[NSMutableDictionary alloc] init];
    _registeredFriendContext = [RegisteredFriendContext contextWithLimit:_limitCount];
    
    [KakaoGameAPI registeredFriendsWithContext:_registeredFriendContext completionHandler:^(NSArray *friends, NSError *error) {
        if (error) {
            NSLog(@"friends error = %@", error);
            dict[@"success"] = @"false";
            dict[@"error"] = [NSString stringWithFormat:@"%ld", (long)error.code];
        } else {
            dict[@"success"] = @"true";
            NSLog(@"friends.count : %lu",(unsigned long)friends.count);
            
            m_InstalledfriendsInfo = [[NSMutableArray alloc] initWithArray:friends];
            unsigned long count = m_InstalledfriendsInfo.count;
            NSMutableArray *kakaoFriendProfile = [[NSMutableArray alloc] initWithCapacity:count];
            for(int i = 0 ;i < count;i++)
            {
                KOFriend *friend = m_InstalledfriendsInfo[i];
                NSMutableDictionary *friendData = [[NSMutableDictionary alloc] init];
                if(friend.ID)
                    friendData[@"userId"] = friend.ID;
                else
                    [friendData setValue:@"0" forKey:@"userId"];
                
                friendData[@"profileNickname"] = RemoveEmoji(friend.nickName);
                friendData[@"uuid"] = friend.uuid;
                friendData[@"profileThumbnailImage"] = friend.thumbnailURL;
                friendData[@"serviceUserId"] =  [friend.extras valueForKey:@"EXTRA_KEY_SERVICE_USER_ID"];
                friendData[@"isAppRegistered"] = (friend.isAppRegistered == true ? @"true":@"false");
                
                if(friend.talkOS == 0)
                    friendData[@"talkOs"] = @"none";
                else if(friend.talkOS == 1)
                    friendData[@"talkOs"] = @"ios";
                else if(friend.talkOS == 2)
                    friendData[@"talkOs"] = @"android";
                
                friendData[@"isAllowedMsg"] = (friend.isAllowedTalkMessaging == true ? @"true":@"false");
                kakaoFriendProfile[i] = friendData;
            }
            dict[@"friendinfo"] = kakaoFriendProfile;
        }
        NSError * err;
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&err];
        NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //        myString = [myString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
        
        UnitySendMessage(UnityGameObjectName, "KakaoInstalledFriendsResponse", MakeStringCopy([myString UTF8String]));
    }];
    
}

void _KakaoRequestNotInstallFriendInfo()
{
    [dict removeAllObjects];
    dict = [[NSMutableDictionary alloc] init];
    _invitableFriendContext = [InvitableFriendContext contextWithLimit:_limitCount];
    [KakaoGameAPI invitableFriendsWithLimit:_invitableFriendContext completionHandler:^(NSArray *friends, NSError *error) {
        if (error) {
            NSLog(@"friends error = %@", error);
            dict[@"success"] = @"false";
            dict[@"error"] = [NSString stringWithFormat:@"%ld", (long)error.code];
        } else {
            dict[@"success"] = @"true";
            NSLog(@"friends.count : %lu",(unsigned long)friends.count);
            
            m_InvitablefriendsInfo = [[NSMutableArray alloc] initWithArray:friends];
            unsigned long count = m_InvitablefriendsInfo.count;
            NSMutableArray *kakaoFriendProfile = [[NSMutableArray alloc] initWithCapacity:count];
            for(int i = 0 ;i < count;i++)
            {
                KOFriend *friend = m_InvitablefriendsInfo[i];
                NSMutableDictionary *friendData = [[NSMutableDictionary alloc] init];
                if(friend.ID)
                    friendData[@"userId"] = friend.ID;
                else
                    [friendData setValue:@"0" forKey:@"userId"];
                
                friendData[@"profileNickname"] = RemoveEmoji(friend.nickName);
                friendData[@"uuid"] = friend.uuid;
                friendData[@"profileThumbnailImage"] = friend.thumbnailURL;
                friendData[@"serviceUserId"] =  [friend.extras valueForKey:@"EXTRA_KEY_SERVICE_USER_ID"];
                friendData[@"isAppRegistered"] = (friend.isAppRegistered == true ? @"true":@"false");
                
                if(friend.talkOS == 0)
                    friendData[@"talkOs"] = @"none";
                else if(friend.talkOS == 1)
                    friendData[@"talkOs"] = @"ios";
                else if(friend.talkOS == 2)
                    friendData[@"talkOs"] = @"android";
                
                friendData[@"isAllowedMsg"] = (friend.isAllowedTalkMessaging == true ? @"true":@"false");
                kakaoFriendProfile[i] = friendData;
            }
            dict[@"friendinfo"] = kakaoFriendProfile;
        }
        NSError * err;
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&err];
        NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //        myString = [myString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
        
        UnitySendMessage(UnityGameObjectName, "KakaoNotInstallFriendsResponse",[myString UTF8String]);
    }];
}

void _KakaoRequestInviteFriend(char * friendUUID)
{
    [dict removeAllObjects];
    dict = [[NSMutableDictionary alloc] init];
    
    if(m_InvitablefriendsInfo.count <= 0)
    {
        dict[@"success"] = @"false";
        dict[@"error"] = @"-998";
        
        NSError * err;
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&err];
        NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        UnitySendMessage(UnityGameObjectName, "KakaoSendInviteMessageResponse", MakeStringCopy([myString UTF8String]));
        return;
    }
    
    KOFriend *targetFriend = NULL;
    unsigned long count = m_InvitablefriendsInfo.count;
    for(int i = 0; i <count ;i++)
    {
        KOFriend *temp = [m_InvitablefriendsInfo objectAtIndex:i];
        if([temp.uuid isEqualToString:[NSString stringWithFormat:@"%s" , friendUUID]])
        {
            targetFriend = temp;
            break;
        }
    }
    
    if(targetFriend == NULL)
    {
        dict[@"success"] = @"false";
        dict[@"error"] = @"-998";
        NSError * err;
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&err];
        NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        UnitySendMessage(UnityGameObjectName, "KakaoSendInviteMessageResponse", MakeStringCopy([myString UTF8String]));
        return;
    }
    
    [KakaoGameAPI sendInviteMessageTaskWithTemplateID:226
                                       receiverFriend:targetFriend
                                     messageArguments:@{@"sender_name" : [_user.properties valueForKey:@"nickname"]}
                                    completionHandler:^(NSError *error) {
                                        if (error) {
                                            dict[@"success"] = @"false";
                                            dict[@"error"] = [NSString stringWithFormat:@"%ld", (long)error.code];
                                            NSError * err;
                                            NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&err];
                                            NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                                            UnitySendMessage(UnityGameObjectName, "KakaoSendInviteMessageResponse", MakeStringCopy([myString UTF8String]));
                                        } else {
                                            dict[@"success"] = @"true";
                                            NSError * err;
                                            NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&err];
                                            NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                                            UnitySendMessage(UnityGameObjectName, "KakaoSendInviteMessageResponse", MakeStringCopy([myString UTF8String]));
                                        }
                                    }];
}

void _KakaoRequestSendGameMessage(char * friendUUID)
{
    [dict removeAllObjects];
    dict = [[NSMutableDictionary alloc] init];
    
    if(m_InstalledfriendsInfo.count <= 0)
    {
        dict[@"success"] = @"false";
        dict[@"error"] = @"-998";
        NSError * err;
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&err];
        NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        UnitySendMessage(UnityGameObjectName, "KakaoSendGameMessageResponse", MakeStringCopy([myString UTF8String]));
        return;
    }
    KOFriend *targetFriend = NULL;
    unsigned long count = m_InstalledfriendsInfo.count;
    for(int i = 0; i <count ;i++)
    {
        KOFriend *temp = [m_InstalledfriendsInfo objectAtIndex:i];
        if([temp.uuid isEqualToString:[NSString stringWithFormat:@"%s" , friendUUID]])
        {
            targetFriend = temp;
            break;
        }
    }
    
    if(targetFriend == NULL)
    {
        dict[@"success"] = @"false";
        dict[@"error"] = @"-998";
        NSError * err;
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&err];
        NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        UnitySendMessage(UnityGameObjectName, "KakaoSendGameMessageResponse", MakeStringCopy([myString UTF8String]));
        return;
    }
    
    [KakaoGameAPI sendInviteMessageTaskWithTemplateID:227
                                       receiverFriend:targetFriend
                                     messageArguments:@{@"sender_name" : [_user.properties valueForKey:@"nickname"]}
                                    completionHandler:^(NSError *error) {
                                        if (error) {
                                            dict[@"success"] = @"false";
                                            dict[@"error"] = [NSString stringWithFormat:@"%ld", (long)error.code];
                                            NSError * err;
                                            NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&err];
                                            NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                                            UnitySendMessage(UnityGameObjectName, "KakaoSendGameMessageResponse", MakeStringCopy([myString UTF8String]));
                                        } else {
                                            dict[@"success"] = @"true";
                                            NSError * err;
                                            NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&err];
                                            NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                                            UnitySendMessage(UnityGameObjectName, "KakaoSendGameMessageResponse", MakeStringCopy([myString UTF8String]));
                                        }
                                    }];
}

void _KakaoBlockMessage()
{
    [dict removeAllObjects];
    dict = [[NSMutableDictionary alloc] init];
    
    [KakaoGameAPI showMessageBlockDialogWithCompletionHandler:^(BOOL success, NSError *error) {
        if (error) {
            dict[@"success"] = @"false";
            dict[@"error"] = [NSString stringWithFormat:@"%ld", (long)error.code];
            NSError * err;
            NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&err];
            NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            UnitySendMessage(UnityGameObjectName, "KakaoBlockMessageResponse", MakeStringCopy([myString UTF8String]));
        } else {
            [KakaoGameAPI meTaskWithCompletionHandler:^(id result, NSError *error) {
                if (error) {
                    NSLog(@"Error Description : %@",error.description);
                    dict[@"success"] = @"false";
                    dict[@"error"] = [NSString stringWithFormat:@"%ld", (long)error.code];
                } else {
                    _user = result;
                    dict[@"success"] = @"true";
                    dict[@"msg_blocked"] = [[NSString stringWithFormat:@"%@", [_user.properties valueForKey:@"msg_blocked"]]    isEqual: @"false"] ? @"false":@"true";
                    
                    NSError * err;
                    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&err];
                    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                    myString = [myString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                    UnitySendMessage(UnityGameObjectName, "KakaoBlockMessageResponse", MakeStringCopy([myString UTF8String]));
                }
            }];
        }
    }];
}

char * MakeStringCopy(const char * string)
{
    if( string == NULL )
        return NULL;
    
    char * res = (char *)malloc( strlen( string ) + 1 );
    strcpy( res, string );
    
    return res;
}

NSString * RemoveEmoji(NSString *text)
{
    __block NSMutableString* temp = [NSMutableString string];
    
    [text enumerateSubstringsInRange:NSMakeRange(0,[text length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:     ^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop){
        const unichar hs = [substring characterAtIndex: 0];
        
        // surrogate pair
        if (0xd800 <= hs && hs <= 0xdbff) {
            const unichar ls = [substring characterAtIndex: 1];
            const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
            
            [temp appendString: (0x1d000 <= uc && uc <= 0x1f77f)? @"": substring]; // U+1D000-1F77F
            
            // non surrogate
        } else {
            [temp appendString: (0x2100 <= hs && hs <= 0x26ff)? @"": substring]; // U+2100-26FF
        }
    }];
    
    return temp;
}


@end
