﻿using UnityEngine;
using System;
using System.Collections;
using KakaoUnityPlugin;

public class KakaoTestGUI : MonoBehaviour
{
    void Awake()
    {
        KakaoManager.KakaoInitCallBack += OnKakaoInit;
        KakaoManager.KakaoLoginCallBack += OnKakaoLogin;
        KakaoManager.KakaoLogOutCallBack += OnKakaoLogOut;
        KakaoManager.KakaoDisconnectionCallBack += OnKakaoDisconnection;
        KakaoManager.KakaoInstalledFriendsCallBack += OnKakaoInstalledFriends;
        KakaoManager.KakaoNotInstallFriendsCallBack += OnKakaoNotInstallFriends;
        KakaoManager.KakaoRequestMeCallBack += OnKakaoRequestMe;
        KakaoManager.KakaoSendGameMessageCallBack += OnKakaoSendGameMessage;
        KakaoManager.KakaoSendInviteMessageCallBack += OnKakaoSendInviteMessage;
        KakaoManager.KakaoSignOutCallBack += OnKakaoSignOut;
        KakaoManager.KakaoBlockMessageCallBack += OnKakaoBlockMessage;
    }
    
    void OnDestroy()
    {
        KakaoManager.KakaoInitCallBack -= OnKakaoInit;
        KakaoManager.KakaoLoginCallBack -= OnKakaoLogin;
        KakaoManager.KakaoLogOutCallBack -= OnKakaoLogOut;
        KakaoManager.KakaoDisconnectionCallBack -= OnKakaoDisconnection;
        KakaoManager.KakaoInstalledFriendsCallBack -= OnKakaoInstalledFriends;
        KakaoManager.KakaoNotInstallFriendsCallBack -= OnKakaoNotInstallFriends;
        KakaoManager.KakaoRequestMeCallBack -= OnKakaoRequestMe;
        KakaoManager.KakaoSendGameMessageCallBack -= OnKakaoSendGameMessage;
        KakaoManager.KakaoSendInviteMessageCallBack -= OnKakaoSendInviteMessage;
        KakaoManager.KakaoSignOutCallBack -= OnKakaoSignOut;
        KakaoManager.KakaoBlockMessageCallBack -= OnKakaoBlockMessage;
    }
    
    void OnKakaoInit(KakaoResponse response)
    {
        Debug.Log("OnKakaoInit : " + response.Success);
    }
    
    void OnKakaoLogin(KakaoResponse response)
    {
        Debug.Log("OnKakaoLogin : " + response.Success);
    }
    
    void OnKakaoLogOut(KakaoResponse response)
    {
        Debug.Log("OnKakaoLogOut : " + response.Success);
    }
    
    void OnKakaoDisconnection(KakaoResponse response)
    {
        Debug.Log("OnKakaoDisconnection : " + response.Success);
    }
    
    void OnKakaoInstalledFriends(KakaoResponse response)
    {
        Debug.Log("KakaoInstalledFriends : " + response.Success);
    }
    
    void OnKakaoNotInstallFriends(KakaoResponse response)
    {
        Debug.Log("KakaoNotInstallFriends : " + response.Success);
    }
    
    void OnKakaoRequestMe(KakaoResponse response)
    {
        Debug.Log("KakaoRequestMe : " + response.Success);
    }
    
    void OnKakaoSendGameMessage(KakaoResponse response)
    {
        Debug.Log("KakaoSendGameMessage : " + response.Success);
    }
    
    void OnKakaoSendInviteMessage(KakaoResponse response)
    {
        Debug.Log("KakaoSendInviteMessage : " + response.Success);
    }
    
    void OnKakaoSignOut(KakaoResponse response)
    {
        Debug.Log("KakaoSignOut : " + response.Success);
    }
    
    void OnKakaoBlockMessage(KakaoResponse response)
    {
        Debug.Log("OnKakaoBlockMessage : " + response.Success);
    }
    
    void OnGUI()
    {
        if (KakaoManager.Instance.Initialized == false)
        {
            if (this.Button("KakaoInit")) 
                KakaoManager.Instance.KakaoInit();
            
            if (this.Button("Login")) 
                KakaoManager.Instance.KakaoLogin();
        }
        
        if (KakaoManager.Instance.Initialized == true)
        {
            if (this.Button("RequestMe")) 
                KakaoManager.Instance.KakaoRequestMe();
            
            if (this.Button("InstalledFriends")) 
                KakaoManager.Instance.KakaoInstalledFriends();
            
            if (this.Button("NotInstallFriends")) 
                KakaoManager.Instance.KakaoNotInstallFriends();
            
            if (this.Button("RequestInviteFriend")) 
                KakaoManager.Instance.KakaoSendInviteMessage("Friend UUID");
            
            if (this.Button("RequestSendGameMessage"))
                KakaoManager.Instance.KakaoSendGameMessage("Friend UUID");
            
            if (this.Button("BlockMessage")) 
                KakaoManager.Instance.KakaoBlockMessage();
            
            if (this.Button("Valid AccessToken")) 
                KakaoManager.Instance.ValidAccessToken();
            
            if (this.Button("LogOut")) 
                KakaoManager.Instance.KakaoLogOut();
            
            if (this.Button("SignOut")) 
                KakaoManager.Instance.KakaoSignOut();
        }
    }
    
    private bool Button(string label)
    {
        return GUILayout.Button(
            label,
            GUILayout.MinHeight(100),
            GUILayout.Width(500)
            );
    }
}