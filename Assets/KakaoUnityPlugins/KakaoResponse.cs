using System;
using System.Collections;
using System.Collections.Generic;
using Pathfinding.Serialization.JsonFx;

namespace KakaoUnityPlugin
{
    /// <summary>
    /// 카카오 응답 정보.
    /// </summary>
    public class KakaoResponse
    {
        public const string s_MessageBlock = "msg_blocked";
        public const string s_UserProfile = "userprofile";
        public const string s_FriendInfo = "friendinfo";
        public const string s_Success = "success";
        public const string s_ErrorCode = "errorcode";

        /// <summary>
        /// 성공 여부.
        /// </summary>
        /// <value>TRUE 성공, FALSE 실패.</value>
        public bool Success
        {
            get;
            private set;
        }

        /// <summary>
        /// 응답 에러코드.
        /// </summary>
        /// <value>에러코드.</value>
        public int? ErrorCode
        {
            get;
            private set;
        }

        /// <summary>
        /// 유저 프로필정보.
        /// </summary>
        /// <value>유저 프로필.</value>
        public KakaoUserProfile UserProfile
        {
            get;
            private set;
        }

        /// <summary>
        /// 친구 정보.
        /// </summary>
        /// <value>친구정보.</value>
        public KakaoFriendInfo[] FriendInfo
        {
            get;
            private set;
        }

        /// <summary>
        /// 메시지 블럭 여부.
        /// </summary>
        /// <value>TRUE 블럭, FALSE 허용.</value>
        public bool? BlockMessage
        {
            get;
            private set;
        }

        /// <summary>
        /// 네이티브로 부터 온 Json String 파싱.
        /// </summary>
        /// <param name="response">Json String.</param>
        /// TODO : 해당 요청에 따른 필요한 데이터만 채워짐. 내정보 요청을 했다고 친구정보가 채워지지 않음. (NULL 임.)
        public KakaoResponse(string response)
        {
            UnityEngine.Debug.Log("KakaoResponse : " + response);
            Dictionary<string,object> responseData = JsonReader.Deserialize<Dictionary<string,object>>(response);
        
            if (responseData.ContainsKey(KakaoResponse.s_Success) == true)
                this.Success = bool.Parse(responseData [KakaoResponse.s_Success].ToString());
            else
                this.Success = false;
        
            if (responseData.ContainsKey(KakaoResponse.s_ErrorCode) == true)
                this.ErrorCode = int.Parse(responseData [KakaoResponse.s_ErrorCode].ToString());
            else
                this.ErrorCode = null;
        
            if (responseData.ContainsKey(KakaoResponse.s_UserProfile) == true)
                this.UserProfile = JsonReader.Deserialize<KakaoUserProfile>(JsonWriter.Serialize(responseData [KakaoResponse.s_UserProfile]));
            else
                this.UserProfile = null;
        
            if (responseData.ContainsKey(KakaoResponse.s_FriendInfo) == true)
                this.FriendInfo = JsonReader.Deserialize<KakaoFriendInfo[]>(JsonWriter.Serialize(responseData [KakaoResponse.s_FriendInfo]));
            else
                this.FriendInfo = null;
        
            if (responseData.ContainsKey(KakaoResponse.s_MessageBlock) == true)
                this.BlockMessage = bool.Parse(responseData [KakaoResponse.s_MessageBlock].ToString());
            else
                this.BlockMessage = null;
        }
    }
}