﻿namespace KakaoUnityPlugin
{
    /// <summary>
    /// 카카오 에러코드.
    /// </summary>
    public enum KakaoError
    {
        NotSignedUp = -998,
        TokenError = -401,
        MonthlyMessageLimit = -531,
        InvitableFriendNotExist = -532,
        InstalledFriendNotExist = -533
    }
}