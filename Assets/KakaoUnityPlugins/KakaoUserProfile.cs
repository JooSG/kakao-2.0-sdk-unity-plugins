﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace KakaoUnityPlugin
{
    /// <summary>
    /// 카카오 유저 프로필 정보.
    /// </summary>
    public class KakaoUserProfile
    {
        /// <summary>
        /// 유저 아이디.
        /// </summary>
        /// <value>아이디.</value>
        public long id
        {
            get;
            private set;
        }

        /// <summary>
        /// 유저 닉네임.
        /// </summary>
        /// <value>닉네임.</value>
        public string nickname
        {
            get;
            private set;
        }

        /// <summary>
        /// 유저 썸네임 사진.
        /// </summary>
        /// <value>썸네임 URL.</value>
        public string thumbnailImagePath
        {
            get;
            private set;
        }

        /// <summary>
        /// 유저 프로필 사진.
        /// </summary>
        /// <value>프로필 사진 URL.</value>
        public string profileImagePath
        {
            get;
            private set;
        }

        /// <summary>
        /// 유저 UUID.
        /// </summary>
        /// <value>UUID.</value>
        public string uuid
        {
            get;
            private set;
        }

        /// <summary>
        /// 유저서비스 아이디.
        /// </summary>
        /// <value>서비스 아이디.</value>
        public long serviceUserId
        {
            get;
            private set;
        }

        /// <summary>
        /// 유저 초대 가능 횟수.
        /// </summary>
        /// <value>초대 가능 횟수.</value>
        public int remainingInviteCount
        {
            get;
            private set;
        }

        /// <summary>
        /// 그룹메시지 가능한 횟수.
        /// </summary>
        /// <value>그룹메시지 가능한 횟수.</value>
        public int remainingGroupMsgCount
        {
            get;
            private set;
        }

        /// <summary>
        /// 유저 커스텀 정보. (메시지 블럭 정보 등등)
        /// </summary>
        /// <value>커스텀정보 Dict.</value>
        public Dictionary<string,string> properties
        {
            get;
            private set;
        }

        public KakaoUserProfile()
        {
        }

        /// <summary>
        /// 유저 메시지 블럭 여부.
        /// </summary>
        /// <value>TRUE 블럭, FALSE 허용.</value>
        public bool BlockedMessage
        {
            get
            {
                if (properties.ContainsKey(KakaoResponse.s_MessageBlock) == true)
                {
                    bool block = false;
                    bool.TryParse(properties [KakaoResponse.s_MessageBlock], out block);

                    return block;
                }
                else
                    return false;
            }set
            {
                properties [KakaoResponse.s_MessageBlock] = value.ToString();
            }
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents the current <see cref="KakaoUserProfile"/>.
        /// </summary>
        /// <returns>A <see cref="System.String"/> that represents the current <see cref="KakaoUserProfile"/>.</returns>
        public override string ToString()
        {
            return string.Format("[KakaoUserProfile: id={0}, nickname={1}, thumbnailImagePath={2}, profileImagePath={3}, uuid={4}, serviceUserId={5}, remainingInviteCount={6}, remainingGroupMsgCount={7}, properties={8}, BlockedMessage={9}]", id, nickname, thumbnailImagePath, profileImagePath, uuid, serviceUserId, remainingInviteCount, remainingGroupMsgCount, properties, BlockedMessage);
        }
    }
}