﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Security;
using System.IO;
using System.Text;
using Pathfinding.Serialization.JsonFx;
using System.Runtime.InteropServices;

namespace KakaoUnityPlugin
{
/// <summary>
/// 카카오 매니저.
/// </summary>
    public class KakaoManager : MonoBehaviour
    {
        /// <summary>
        /// 로컬에 저장되는 초대보낸 친구정보의 키값.
        /// </summary>
        private const string InvitedFriend_PlayerPrefsKeyName = "InvitedFriend";

        public delegate void KakaoCallBack(KakaoResponse response);

        /// <summary>
        /// 카카오 초기화 콜백, 자동로그인 판단.
        /// </summary>
        public static event KakaoCallBack KakaoInitCallBack;
        /// <summary>
        /// 카카오 로그인 콜백.
        /// </summary>
        public static event KakaoCallBack KakaoLoginCallBack;
        /// <summary>
        /// 카카오 로그인아웃 콜백. (통상 앱의 스플래쉬 화면으로 돌아가는게 좋다. 카카오 로그인 다시 시도.)
        /// </summary>
        public static event KakaoCallBack KakaoLogOutCallBack;
        /// <summary>
        /// 카카오 게임 탈퇴 콜백. (통상 앱의 스플래쉬 화면으로 돌아가는게 좋다. 카카오 로그인 다시 시도.)
        /// </summary>
        public static event KakaoCallBack KakaoSignOutCallBack;
        /// <summary>
        /// 카카오 유저정보 요청 콜백.
        /// </summary>
        public static event KakaoCallBack KakaoRequestMeCallBack;
        /// <summary>
        /// 게임이 설치가 된 친구정보 요청 콜백.
        /// </summary>
        public static event KakaoCallBack KakaoInstalledFriendsCallBack;
        /// <summary>
        /// 게임 미설치가 된 친구정보 요청 콜백.
        /// </summary>
        public static event KakaoCallBack KakaoNotInstallFriendsCallBack;
        /// <summary>
        /// 게임 초대 메시지 요청 콜백.
        /// </summary>
        public static event KakaoCallBack KakaoSendInviteMessageCallBack;
        /// <summary>
        /// 게임 메시지 요청 콜백.
        /// </summary>
        public static event KakaoCallBack KakaoSendGameMessageCallBack;
        /// <summary>
        /// 게임 메시지 수신 여부 콜백.
        /// </summary>
        public static event KakaoCallBack KakaoBlockMessageCallBack;
        /// <summary>
        /// 계정이 카카오와 연결이 끊겼을때 콜백. (통상 앱의 스플래쉬 화면으로 돌아가는게 좋다. 카카오 로그인 다시 시도.)
        /// </summary>
        public static event KakaoCallBack KakaoDisconnectionCallBack;

        private static KakaoManager instance;
    
        public static KakaoManager Instance
        { 
            get
            { 
                if (null == instance)
                    instance = FindObjectOfType(typeof(KakaoManager)) as KakaoManager; 
            
            
                if (null == instance)
                { 
                    GameObject go = new GameObject(); 
                    instance = go.AddComponent<KakaoManager>(); 
                    instance.name = "KakaoManager";
                } 
                return instance; 
            }
        }

        /// <summary>
        /// 카카오 유저 정보.
        /// </summary>
        public KakaoUserProfile UserProfile
        {
            get;
            private set;
        }
    
        /// <summary>
        /// 게임이 설치된 친구 정보.
        /// </summary>
        public KakaoFriendInfo[] InstalledFriendInfo
        {
            get;
            private set;
        }
    
        /// <summary>
        /// 게임이 미설치된 친구 정보.
        /// </summary>
        public KakaoFriendInfo[] NotInstallFriendInfo
        {
            get;
            private set;
        }
    
        /// <summary>
        /// 게임에 초대한 친구 정보. (Key = 친구 UUID, Value = 다시 초대메시지를 보낼 수 있는 날짜.)
        /// </summary>
        public Dictionary<string, DateTime> InvitedFriendInfo
        {
            get;
            private set;
        }

        /// <summary>
        /// 카카오SDK Initialized 여부.
        /// </summary>
        /// <value>TRUE 면 성공, FALSE 면 실패.</value>
        public bool Initialized
        {
            get;
            private set;
        }

        /// <summary>
        /// 엑세스 토큰.
        /// </summary>
        private string m_AccessToken;
        /// <summary>
        /// 엑세스 토큰 유효성 체크 URL.
        /// </summary>
        private string ValidCheckAccessTokenURL;
        /// <summary>
        /// 카카오 REST API Header
        /// </summary>
        private string m_KAHeader;
        /// <summary>
        /// 게임 초대메시지를 보낸 친구 UUID.
        /// </summary>
        private string m_SendInviteMessageFriendID;

        #if UNITY_ANDROID
        private AndroidJavaClass m_AndroidJavaClass;
        private AndroidJavaObject m_AndroidJavaObject;
        #elif UNITY_IPHONE
        [DllImport("__Internal")]
        private static extern string _GetCFBundleVersion();
        [DllImport("__Internal")]
        private static extern bool _KakaoInit();
        [DllImport("__Internal")]
        private static extern void _KakaoLogin();
        [DllImport("__Internal")]
        private static extern void _KakaoLogOut();
        [DllImport("__Internal")]
        private static extern void _KakaoSignOut();
        [DllImport("__Internal")]
        private static extern void _KakaoRequestMe();
        [DllImport("__Internal")]
        private static extern void _KakaoRequestInstalledFriendInfo();
        [DllImport("__Internal")]
        private static extern void _KakaoRequestNotInstallFriendInfo();
        [DllImport("__Internal")]
        private static extern void _KakaoRequestInviteFriend();
        [DllImport("__Internal")]
        private static extern void _KakaoRequestSendGameMessage();
        [DllImport("__Internal")]
        private static extern void _KakaoBlockMessage();
        #endif
    
        void Awake()
        {
            if (instance != null)
            {  
                Destroy(this.gameObject);
                return;
            }
        
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

        void Start()
        {
            // 서버 인증서의 유효성 설정.
            System.Net.ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => {
                return true; };
            #if UNITY_ANDROID
            this.m_AndroidJavaClass = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
            this.m_AndroidJavaObject = this.m_AndroidJavaClass.GetStatic<AndroidJavaObject> ("currentActivity");
            #endif
        
            LoadInvitedFriend();
        }

        /// <summary>
        /// 카카오 로그인 요청.
        /// </summary>
        public void KakaoInit()
        {
            #if UNITY_ANDROID
            this.m_AndroidJavaObject.Call ("KakaoInit");
            #elif UNITY_IPHONE
            KakaoManager._KakaoInit();
            #endif
        }

        /// <summary>
        /// 카카오 로그인 요청.
        /// </summary>
        public void KakaoLogin()
        {
            #if UNITY_ANDROID
            this.m_AndroidJavaObject.Call ("Login");
            #elif UNITY_IPHONE
            KakaoManager._KakaoLogin();
            #endif
        }
    
        /// <summary>
        /// 카카오 로그아웃 요청.
        /// </summary>
        public void KakaoLogOut()
        {
            #if UNITY_ANDROID
            this.m_AndroidJavaObject.Call ("LogOut");
            #elif UNITY_IPHONE
            KakaoManager._KakaoLogOut();
            #endif
        }

        /// <summary>
        /// 게임 탈퇴 요청.
        /// </summary>
        public void KakaoSignOut()
        {
            #if UNITY_ANDROID
            this.m_AndroidJavaObject.Call ("SignOut");
            #elif UNITY_IPHONE
            KakaoManager._KakaoSignOut();
            #endif
        }

        /// <summary>
        /// 유저정보 요청.
        /// </summary>
        public void KakaoRequestMe()
        {
            #if UNITY_ANDROID
            this.m_AndroidJavaObject.Call("RequestMe");
            #elif UNITY_IPHONE
            KakaoManager._KakaoRequestMe();        
            #endif
        }

        /// <summary>
        /// 게임이 설치된 친구정보 요청.
        /// </summary>
        public void KakaoInstalledFriends()
        {
            #if UNITY_ANDROID
            this.m_AndroidJavaObject.Call ("RequestInstalledFriendInfo");
            #elif UNITY_IPHONE
            KakaoManager._KakaoRequestInstalledFriendInfo();
            #endif
        }

        /// <summary>
        /// 게임이 미설치된 친구정보 요청.
        /// </summary>
        public void KakaoNotInstallFriends()
        {
            #if UNITY_ANDROID
            this.m_AndroidJavaObject.Call("RequestNotInstallFriendInfo");
            #elif UNITY_IPHONE
            KakaoManager._KakaoRequestNotInstallFriendInfo();
            #endif
        }

        /// <summary>
        /// 게임 초대 메시지 요청.
        /// </summary>
        public void KakaoSendInviteMessage(string friendUUID)
        {
            if (string.IsNullOrEmpty(friendUUID) == true)
                Debug.LogError("friendUUID is Empty");

            this.m_SendInviteMessageFriendID = friendUUID;
            #if UNITY_ANDROID
            this.m_AndroidJavaObject.Call ("RequestInviteFriend", friendUUID);
            #elif UNITY_IPHONE
            KakaoManager._KakaoRequestInviteFriend();
            #endif
        }

        /// <summary>
        /// 게임 메시지 요청.
        /// </summary>
        public void KakaoSendGameMessage(string friendUUID)
        {
            if (string.IsNullOrEmpty(friendUUID) == true)
                Debug.LogError("friendUUID is Empty");

            #if UNITY_ANDROID
            this.m_AndroidJavaObject.Call ("RequestSendGameMessage", friendUUID);
            #elif UNITY_IPHONE
            KakaoManager._KakaoRequestSendGameMessage();
            #endif
        }

        /// <summary>
        /// 메시지 수신 여부 요청.
        /// </summary>
        public void KakaoBlockMessage()
        {
            #if UNITY_ANDROID
            this.m_AndroidJavaObject.Call("BlockMessage");
            #elif UNITY_IPHONE
            KakaoManager._KakaoBlockMessage();
            #endif
        }

        /// <summary>
        /// 카카오 초기화 응답.
        /// </summary>
        /// <param name="response">TRUE 면 자동로그인, 통상 유저정보를 불러온다. / FALSE 면 로그인버튼 노출</param>
        private void KakaoInitResponse(string response)
        {
            KakaoResponse kakaoResponse = new KakaoResponse(response);
            this.Initialized = kakaoResponse.Success;

            if (KakaoInitCallBack != null)
                KakaoInitCallBack(kakaoResponse);
        }

        /// <summary>
        /// 카카오 로그인 응답.
        /// </summary>
        /// <param name="response">성공시 TRUE / 실패시 FALSE</param>
        private void KakaoLoginResponse(string response)
        {
            KakaoResponse kakaoResponse = new KakaoResponse(response);
            this.Initialized = kakaoResponse.Success;

            if (KakaoLoginCallBack != null)
                KakaoLoginCallBack(kakaoResponse);
        }

        /// <summary>
        /// 카카오 로그아웃 응답.
        /// </summary>
        /// <param name="response">성공시 TRUE / 실패시 FALSE</param>
        private void KakaoLogOutResponse(string response)
        {
            KakaoResponse kakaoResponse = new KakaoResponse(response);
            if (kakaoResponse.Success == true)
                this.Initialized = false;

            if (KakaoLogOutCallBack != null)
                KakaoLogOutCallBack(kakaoResponse);
        }

        /// <summary>
        /// 게임탈퇴 응답.
        /// </summary>
        /// <param name="response">성공시 TRUE / 실패시 FALSE</param>
        private void KakaoSignOutResponse(string response)
        {
            KakaoResponse kakaoResponse = new KakaoResponse(response);
            if (kakaoResponse.Success == true)
                this.Initialized = false;

            if (KakaoSignOutCallBack != null)
                KakaoSignOutCallBack(kakaoResponse);
        }

        /// <summary>
        /// 유저정보 요청 응답.
        /// </summary>
        /// <param name="response">성공시 유저정보 JsonString / 실패시 NULL</param>
        private void KakaoRequestMeResponse(string response)
        {
            KakaoResponse kakaoResponse = new KakaoResponse(response);
            if (kakaoResponse.Success == true)
            {
                this.UserProfile = kakaoResponse.UserProfile;
                this.Initialized = kakaoResponse.Success;
            }

            if (KakaoRequestMeCallBack != null)
                KakaoRequestMeCallBack(kakaoResponse);
        }

        /// <summary>
        /// 게임이 설치된 친구 요청 응답.
        /// </summary>
        /// <param name="response">성공시 친구정보 JsonString / 실패시 NULL</param>
        private void KakaoInstalledFriendsResponse(string response)
        {
            KakaoResponse kakaoResponse = new KakaoResponse(response);

            if (kakaoResponse.Success == true)
                this.InstalledFriendInfo = kakaoResponse.FriendInfo;

            if (KakaoInstalledFriendsCallBack != null)
                KakaoInstalledFriendsCallBack(kakaoResponse);
        }

        /// <summary>
        /// 게임이 미설치된 친구 요청 응답.
        /// </summary>
        /// <param name="response">성공시 친구정보 JsonString / 실패시 NULL</param>
        private void KakaoNotInstallFriendsResponse(string response)
        {
            KakaoResponse kakaoResponse = new KakaoResponse(response);

            if (kakaoResponse.Success == true)
            {
                this.NotInstallFriendInfo = kakaoResponse.FriendInfo;
//            for (int i = 0; i<this.NotInstallFriendInfo.Length; i++)
//                Debug.Log(this.NotInstallFriendInfo [i].ToString());
            }

            if (KakaoNotInstallFriendsCallBack != null)
                KakaoNotInstallFriendsCallBack(kakaoResponse);
        }
    
        /// <summary>
        /// 게임초대 메시지 요청 응답.
        /// </summary>
        /// <param name="response">성공시 TRUE / 실패시 FALSE</param>
        private void KakaoSendInviteMessageResponse(string response)
        {
            KakaoResponse kakaoResponse = new KakaoResponse(response);

            if (kakaoResponse.Success == true)
            {
                if (string.IsNullOrEmpty(this.m_SendInviteMessageFriendID) == false)
                {
                    this.InvitedFriendInfo [this.m_SendInviteMessageFriendID] = System.DateTime.UtcNow.AddDays(31);
                    SaveInvitedFriend();
                }
            }

            if (KakaoSendInviteMessageCallBack != null)
                KakaoSendInviteMessageCallBack(kakaoResponse);
        }
    
        /// <summary>
        /// 게임 메시지 요청 응답.
        /// </summary>
        /// <param name="response">성공시 TRUE / 실패시 FALSE</param>
        private void KakaoSendGameMessageResponse(string response)
        {
            KakaoResponse kakaoResponse = new KakaoResponse(response);
        
            if (KakaoSendGameMessageCallBack != null)
                KakaoSendGameMessageCallBack(kakaoResponse);
        }
    
        /// <summary>
        /// 게임 메시지 수신 여부 응답.
        /// </summary>
        /// <param name="response">성공시 TRUE / 실패시 FALSE</param>
        private void KakaoBlockMessageResponse(string response)
        {
            KakaoResponse kakaoResponse = new KakaoResponse(response);

            if (kakaoResponse.Success == true && kakaoResponse.BlockMessage != null && this.UserProfile != null)
                this.UserProfile.BlockedMessage = kakaoResponse.BlockMessage.Value;

            if (KakaoBlockMessageCallBack != null)
                KakaoBlockMessageCallBack(kakaoResponse);
        }

        /// <summary>
        /// 카카오와 연결이 끊겼을때 응답.
        /// </summary>
        /// <param name="response">성공시 TRUE / 실패시 FALSE</param>
        private void KakaoNotSignedUpResponse(string response)
        {
            KakaoResponse kakaoResponse = new KakaoResponse(response);

            if (KakaoDisconnectionCallBack != null)
                KakaoDisconnectionCallBack(kakaoResponse);
        }

        /// <summary>
        /// 엑세스 토큰 유효성 검사.
        /// </summary>
        /// <returns>TRUE 면 성공. FALSE 면 실패.</returns>
        public bool ValidAccessToken()
        {
            if (this.m_AccessToken == null)
            {
                Debug.LogWarning("AccessToken NULL");
                return false;
            }

            HttpWebRequest request;
            request = WebRequest.Create(this.ValidCheckAccessTokenURL) as HttpWebRequest;  
            request.Method = "GET";  
            request.ContentType = "application/x-www-form-urlencoded;charset=utf-8";  
            request.Headers.Add("Authorization", string.Format("Bearer {0}", this.m_AccessToken));
            request.Headers.Add("KA", this.m_KAHeader);
            Debug.Log(this.m_AccessToken);

            HttpWebResponse response;
            string result = null;
            bool success = false;
            try
            {
                using (response = request.GetResponse() as HttpWebResponse)
                {
                    Debug.Log("response.StatusCode : " + response.StatusCode);
                    Stream dataStream = response.GetResponseStream();  
                    StreamReader reader = new StreamReader(dataStream, System.Text.Encoding.GetEncoding("UTF-8"), true);
                    result = reader.ReadToEnd();
                    success = true;
                }
            } catch
            {
                success = false;
            } 
            Debug.Log(result);
            return success;
        }

        /// <summary>
        /// 네이티브로 부터 엑세스토큰을 얻어옴.
        /// </summary>
        /// <param name="accessToken">엑세스토큰.</param>
        private void GetKakaoAccessToken(string accessToken)
        {
            Debug.Log(accessToken);
            this.m_AccessToken = accessToken;
        }

        /// <summary>
        /// 네이티브로 부터 엑세스토큰 URL 얻어옴.
        /// </summary>
        /// <param name="url">URL.</param>
        private void GetValidCheckAccessTokeURL(string url)
        {
            Debug.Log(url);
            this.ValidCheckAccessTokenURL = url;
        }

        /// <summary>
        /// 네이티브로 부터 REST API 헤더를 얻어옴.
        /// </summary>
        /// <param name="kaHeader">Ka header.</param>
        private void GetKakaoKAHeader(string kaHeader)
        {
            Debug.Log(kaHeader);
            this.m_KAHeader = kaHeader;
        }

        /// <summary>
        /// 로컬에서 초대보낸 친구 로드.
        /// </summary>
        private void LoadInvitedFriend()
        {
            if (PlayerPrefs.HasKey(KakaoManager.InvitedFriend_PlayerPrefsKeyName) == true)
            {
                string invitedFriendString = PlayerPrefs.GetString(KakaoManager.InvitedFriend_PlayerPrefsKeyName);
                byte[] invitedFriendByte = Convert.FromBase64String(invitedFriendString);
                string invitedFriendJsonString = Encoding.UTF8.GetString(invitedFriendByte);
                InvitedFriendInfo = JsonReader.Deserialize<Dictionary<string, DateTime>>(invitedFriendJsonString);
                Dictionary<string, DateTime>.Enumerator iter = InvitedFriendInfo.GetEnumerator();
            
                while (iter.MoveNext() == true)
                {
                    if ((int)(iter.Current.Value - DateTime.UtcNow).TotalSeconds < 0)
                        InvitedFriendInfo.Remove(iter.Current.Key);
                }
                SaveInvitedFriend();
            }
            else 
                InvitedFriendInfo = new Dictionary<string, DateTime>();
        }

        /// <summary>
        /// 로컬에 초대보낸 친구정보 저장.
        /// </summary>
        private void SaveInvitedFriend()
        {
            byte[] invitedFriendByte = Encoding.UTF8.GetBytes(JsonWriter.Serialize(InvitedFriendInfo));
            PlayerPrefs.SetString(KakaoManager.InvitedFriend_PlayerPrefsKeyName, Convert.ToBase64String(invitedFriendByte));
        }
    }
}