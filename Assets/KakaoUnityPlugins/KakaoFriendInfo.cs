﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace KakaoUnityPlugin
{
    /// <summary>
    /// 카카오 친구정보.
    /// </summary>
    public class KakaoFriendInfo
    {   

        /// <summary>
        /// 친구 UUID.
        /// </summary>
        /// <value>UUID.</value>
        public string uuid
        {
            get;
            private set;
        }

        /// <summary>
        /// 친구 userID.
        /// </summary>
        /// <value>userID.</value>
        public long userId
        {
            get;
            private set;
        }

        /// <summary>
        /// 친구 서비스 아이디.
        /// </summary>
        /// <value>서비스 아이디.</value>
        public long serviceUserId
        {
            get;
            private set;
        }

        /// <summary>
        /// 친구 게임 설치여부.
        /// </summary>
        /// <value>TRUE 면 설치, FALSE 면 미설치.</value>
        public bool isAppRegistered
        {
            get;
            private set;
        }

        /// <summary>
        /// 친구 닉네임.
        /// </summary>
        /// <value>닉네임.</value>
        public string profileNickname
        {
            get;
            private set;
        }

        /// <summary>
        /// 친구 프로필 썸네임 사진.
        /// </summary>
        /// <value>사진 URL.</value>
        public string profileThumbnailImage
        {
            get;
            private set;
        }

        /// <summary>
        /// 카카오톡 OS.
        /// </summary>
        /// <value>OS.</value>
        public string talkOs
        {
            get;
            private set;
        }

        /// <summary>
        /// 메시지 블럭 여부.
        /// </summary>
        /// <value>TRUE 면 메시지 블럭, FALSE 면 메시지 허용.</value>
        public bool isAllowedMsg
        {
            get;
            private set;
        }

        public KakaoFriendInfo()
        {
        }

        /// <summary>
        /// 친구와의 관계.
        /// </summary>
        /// <value>관계.</value>
        /*public FriendRelation relation
        {
            get;
            private set;
        }*/

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents the current <see cref="KakaoFriendInfo"/>.
        /// </summary>
        /// <returns>A <see cref="System.String"/> that represents the current <see cref="KakaoFriendInfo"/>.</returns>
        public override string ToString()
        {
            return string.Format("[KakaoFriendInfo: uuid={0}, userId={1}, serviceUserId={2}, isAppRegistered={3}, profileNickname={4}, profileThumbnailImage={5}, talkOs={6}, isAllowedMsg={7}", uuid, userId, serviceUserId, isAppRegistered, profileNickname, profileThumbnailImage, talkOs, isAllowedMsg);
        }
    }
    /// <summary>
    /// 친구와 관계정보.
    /// </summary>
    public class FriendRelation
    {
        /// <summary>
        /// 카카오톡 관계.
        /// </summary>
        /// <value>관계.</value>
        public Relation talk
        {
            get;
            private set;
        }

        /// <summary>
        /// 카카오스토리 관계.
        /// </summary>
        /// <value>카카오스토리 관계.</value>
        public Relation story
        {
            get;
            private set;
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents the current <see cref="FriendRelation"/>.
        /// </summary>
        /// <returns>A <see cref="System.String"/> that represents the current <see cref="FriendRelation"/>.</returns>
        public override string ToString()
        {
            return string.Format("[FriendRelation: talk={0}, story={1}]", talk, story);
        }
    }

    /// <summary>
    /// 친구와 관계 종류.
    /// </summary>
    public enum Relation
    {
        NONE,
        FRIEND,
        NOT_FRIEND
    }
}