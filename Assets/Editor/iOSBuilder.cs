﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
using System.Collections.Generic;
using System.IO;

// ref: https://github.com/animetrics/PlistCS
using PlistCS;

// ref: https://github.com/gforeman/XcodeProjectModifier
using UnityEditor.XCodeEditor;

namespace UnityEditor.XCodeDeitor
{
    public static class XCodePostProcess
    {
        [PostProcessBuild]
        public static void OnPostProcessBuild(BuildTarget target, string path)
        {
#if UNITY_IPHONE
            iOSBuilder.EdittingPbxProj(path);
            iOSBuilder.EdittingInfoPlist(Path.Combine(path, "Info.plist"));
#endif
        }
    }
}

public static class PlistValue
{
    public static string APP_ICON_76 = "Icon-76.png";
    public static string APP_ICON_120 = "Icon-60@2x.png";
    public static string APP_ICON_152 = "Icon-76@2x.png";
    public static string URL_TYPE_ID = "kakaocddb7cd52ac6a351360b10099d80b040";
    public static string URL_TYPE_ROLE = "Editor";
}

public class iOSBuilder
{
    // ref: /Users/eunpyoungkim/Library/MobileDevice/Provisioning Profiles
    const string PROVISIONING_UUID = "978518f8-7431-4f54-b889-xxxxxxxxxxxx";
    const string CODE_SIGN_NAME = "iPhone Distribution: APPLE, Inc.";

    public static void EdittingPbxProj(string pathToBuiltProject)
    {
        Debug.LogWarning("EdittingPbxProj");
        // Xcode 프로젝트 불러오기.(project.pbxproj 수정 용도.)
        XCProject prj = new XCProject(pathToBuiltProject);

        // .projmods적용. 외부 프래임워크 추가, 파일 복사(폴더).
        string[] files = Directory.GetFiles(Application.dataPath + "/Editor/XcodeProjectMods", "*.projmods", SearchOption.AllDirectories);
        foreach (string file in files)
            prj.ApplyMod(Application.dataPath, file);

        // 테스트로 아무 프로비저닝이나 추가함.
//        prj.overwriteBuildSetting("CODE_SIGN_NAME_IDENTITY[sdk=iphoneos*]", CODE_SIGN_NAME, "Release");
//        prj.overwriteBuildSetting("CODE_SIGN_NAME_IDENTITY[sdk=iphoneos*]", CODE_SIGN_NAME, "Debug");
//        prj.overwriteBuildSetting("CODE_SIGN_NAME_IDENTITY", CODE_SIGN_NAME, "Release");
//        prj.overwriteBuildSetting("CODE_SIGN_NAME_IDENTITY", CODE_SIGN_NAME, "Debug");
//        prj.overwriteBuildSetting("PROVISIONING_PROFILE", PROVISIONING_UUID, "Release");
//        prj.overwriteBuildSetting("PROVISIONING_PROFILE", PROVISIONING_UUID, "Debug");
    
        // 설정이 변경된 프로젝트 저장.
        prj.Save();
    }

    public static void EdittingInfoPlist(string plistPath)
    {
        // plist 읽기.
        var plst = (Dictionary<string, object>)Plist.readPlist(plistPath);

        // App Icon 설정.
//        var CFBundleIconFiles = (List<object>)plst["CFBundleIconFiles"];
//        CFBundleIconFiles.Add(PlistValue.APP_ICON_76);
//        CFBundleIconFiles.Add(PlistValue.APP_ICON_120);
//        CFBundleIconFiles.Add(PlistValue.APP_ICON_152);

        // URL scheme 설정.
        plst ["CFBundleURLTypes"] = new List<object>{
            new Dictionary<string, object> {
                {"CFBundleTypeRole", PlistValue.URL_TYPE_ROLE},
                {"CFBundleURLName", PlistValue.URL_TYPE_ID},
                {"CFBundleURLSchemes", new List<object>{PlistValue.URL_TYPE_ID}},
            }
        };

        // 추가적인 Info.plist 설정.
        plst["KAKAO_APP_KEY"] = "cddb7cd52ac6a351360b10099d80b040";

        plst ["LSApplicationQueriesSchemes"] = new List<object>{
            "kakaocddb7cd52ac6a351360b10099d80b040",
            "kakaokompassauth",
            "storykompassauth",
            "kakaolink",
            "kakaotalk-4.5.0",
            "kakaostory-2.9.0"
        };

        Dictionary<string,object> NSExceptionDomains = new Dictionary<string, object>();
        Dictionary<string,object> value = new Dictionary<string,object >{
            {"NSExceptionAllowsInsecureHTTPLoads",true},
            {"NSExceptionRequiresForwardSecrecy",false},
            {"NSIncludesSubdomains",true}
        };
        NSExceptionDomains.Add("kakao.co.kr", value);
        NSExceptionDomains.Add("kakao.com", value);
        NSExceptionDomains.Add("kakaocdn.net", value);
        plst ["NSAppTransportSecurity"] = new Dictionary<string,object>
        {
            {"NSExceptionDomains", NSExceptionDomains}
        };

        // plist 덮어 씌우기
        Plist.writeXml(plst, plistPath);
    }
}
