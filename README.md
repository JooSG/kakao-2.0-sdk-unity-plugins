# README #

카카오 2.0 SDK 유니티 플러그인.

### 정보 ###

* 카카오 Android, iOS 네이티브를 유니티에서 호출 할 수 있도록 구현.
* REST API 를 사용하지 않음.

### 설정 ###

##### iOS #####

* 이 프로젝트의 경우 PostBuildProccess 를 이용해 카카오 SDK에서 필요한 모든 요소를 XCODE 에 자동으로  
추가되도록 구현되어있음. PostBuildProccess 를 그대로 사용 할 경우  
Assets => Editor => iOSBuilder 안 내용을 아래 2 ~ 4번 항목을 보고 각 프로젝트에 맞게 수정해서 사용.
* Info.plist 에 카카오 SDK에 필요한 값들을 추가. (자세한 사항은 카카오 가이드 참고.)
* URL Schemes 에는 kakaoXXXXXXXXXX (XXXX 앱키)
* xcode 프로젝트 info에 Key값 KAKAO_APP_KEY, Value값 앱키 설정.
* 자세한 사항은 카카오게임센터 => 가이드 => 개발(2.0) => 개발 환경 설정 참조.

##### Android #####

* Assets/Plugins/Adnroid/res/values/kakao_strings 에  
kakao_app_key, kakao_scheme, kakaolink_host 추가. (자세한건 플러그인 프로젝트 참조)
* AndroidManifest 파일은 플러그인 프로젝트 및 아래 카카오게임센터 가이드 참조.

### 사용방법 및 사양 ###

* Assets/KakaoUnityPlugins/Demo/KakaoDemo씬 확인.
* KakaoManager.cs 에 각종 콜백함수 등록. (미등록 시 각종 카카오 기능실행 후 결과처리를 할 수 없음.)
* 카카오 기능들을 최대한 수동적으로 구현했으며, 콜백의 에러메시지 마다 추가적인 기능은 각 프로젝트에 맞게 클라이언트 개발자가 구현해야함.  
예를 들어 카카오톡 로그인이 성공하면 기본적으로 자기자신 / 친구정보를 불러와야 하는게 맞지만 로그인처리만 되도록 구현되어있음. 로그인이 성공적으로 끝나면 자기자신 / 친구정보를 따로 호출해야함.